# gemini redlink checker

import std/uri
import std/net
import std/options
import std/sequtils
from std/parseutils import parseInt
from std/strutils import split, strip, find, Whitespace
from os import commandLineParams

let timeout = 10000

type
  UnknownScheme = ref object of ValueError
    scheme: string
  Status = tuple[n: byte, meta: string]
  Got = ref tuple[u: Uri, s: Option[Status], e: Option[ref Exception]]
  State = tuple
    got: seq[Got]
    prefix: string

func newState(): ref State =
  new result
  result.got = @[]
  result.prefix = ""
    
proc geminiGet(u: Uri): Socket =
  assert u.scheme == "gemini"
  let socket = newSocket()
  let ctx = newContext(protSSLv23, CVerifyNone)
  wrapSocket(ctx, socket)
  var port: int = 1965
  if u.port != "":
    discard parseInt(u.port, port)
  socket.connect(u.hostname, Port(port))
  # some servers have problems if we split this into two sends
  send(socket, $u & "\r\n")
  return socket

proc parseStatus(s: string): Status =
  let parts = s.split(" ", 1)
  var r: Status
  var tmp: int
  # TODO: handle invalid integers?
  discard parseInt(parts[0], tmp)
  r.n = byte(tmp)
  r.meta = parts[1]
  return r

  
proc parseLink(s: string): string =
  if len(s) <= 2:
    return ""
  if s[0] != '=' or s[1] != '>':
    return ""
  let parts = s[2..len(s)-1].strip().split(Whitespace, 1)
  return parts[0]

func isRedir(n: byte): bool =
  return 30 <= n and n <= 39

func isPrefix(p: string, s: string): bool =
  return s.find(p, 0, len(p)) == 0

proc visited(state: ref State, u: Uri): bool =
  for got in state.got:
    if $got.u == $u:
      return true
    #echo $got.u, "!=", $u
  return false

proc visit(state: ref State, u: Uri, depth: int = 10) =
  if state.visited(u):
    echo "already checked ", $u
    return # don't visit site more than once
  var got = new Got
  got[] = (u, none[Status](), none[ref Exception]())
  insert(state.got, [got])
  try:
    if u.scheme != "gemini":
      var err = new UnknownScheme
      err.scheme = u.scheme
      err.msg = "unknown scheme " & u.scheme
      raise err
    echo "getting ", $u
    let socket = geminiGet(u)
    defer: close(socket)
    var ln: string
    readLine(socket, ln, timeout)
    echo "got ", ln, " from ", $u
    let status = parseStatus(ln)
    got.s = some(status)
    if depth > 0 and isPrefix(state.prefix, $u):
      if isRedir(status.n):
        state.visit(combine(u, parseUri(status.meta)), depth - 1)
      else:
        while ln != "":
          readLine(socket, ln, timeout)
          let link = parseLink(ln)
          if link != "":
            state.visit(combine(u, parseUri(link)), depth - 1)
  except:
    let e = getCurrentException()
    let msg = getCurrentExceptionMsg()
    echo "exception visiting ", u, ": ", msg
    got.e = some(e)

proc report(state: ref State) =
  var nok = 0
  var nex = 0
  var nexscheme = 0
  var nredir = 0
  var nredirperm = 0
  var nbad = 0
  for got in state.got:
    if isSome(got.e):
      nex += 1
      if got.e.get() of UnknownScheme:
        nexscheme += 1
    else:
      # if there is no status but also no exception, something has gone wrong
      let s = got.s.get()
      if s.n == 20:
        nok += 1
      elif isRedir(s.n):
        nredir += 1
        if s.n == 31:
           nredirperm += 1
      else:
        nbad += 1
  echo "=== RESULTS ==="
  echo len(state.got), " links checked"
  echo " ", nex, " threw exceptions"
  echo "  ", nexscheme, " unknown url schemes"
  echo "  ", nex-nexscheme, " other exceptions"
  echo " ", nok+nbad+nredir, " returned a status"
  echo "  ", nok, " ok statuses"
  echo "  ", nredir, " redirect statuses"
  echo "   ", nredirperm, " permenent redirect statuses"
  echo "  ", nbad, " other statuses"


# TODO: allow archiving of linked sites
#[proc archive(state: ref State, path: string) =
  for got in state.got:
    if isSome(got.s):
      let src = geminiGet(
  encodeUrl(
]#

for arg in commandLineParams():
  var state = newState()
  state.prefix = arg
  try:
    state.visit(parseUri(arg))
  finally:
    state.report()
